import { View } from '@nativescript/core';
export declare class LocalVideo extends View {
    localVideoView: any;
    _videoViewDelegate: any;
    nativeView: any;
    constructor();
    createNativeView(): any;
    disposeNativeView(): void;
}
