import { View } from '@nativescript/core';
export declare class RemoteVideo extends View {
    remoteVideoView: any;
    _remoteViewDelegate: any;
    nativeView: UIView;
    constructor();
    createNativeView(): any;
    disposeNativeView(): void;
    get ios(): any;
}
