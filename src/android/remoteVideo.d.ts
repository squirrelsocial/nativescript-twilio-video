import { View } from '@nativescript/core';
export declare class RemoteVideo extends View {
    remoteVideoView: any;
    constructor();
    get android(): any;
    createNativeView(): any;
    disposeNativeView(): void;
}
