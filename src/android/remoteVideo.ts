import { View, Utils } from '@nativescript/core';

declare var com, android: any;
// const VideoView: any = com.twilio.video.VideoView;
// const videoView = new VideoView(Utils.ad.getApplicationContext());

export class RemoteVideo extends View {

    remoteVideoView: any;

    constructor() {
        super();

        this.remoteVideoView = new com.twilio.video.VideoView(Utils.ad.getApplicationContext());
    }

    get android(): any {

        return this.nativeView;

    }

    public createNativeView() {

        return this.remoteVideoView;

    }


    public disposeNativeView() {

        this.nativeView = null;

    }


}