import { View, Utils } from '@nativescript/core';

// var app = require("application");

declare var com, android: any;

// const VideoView: any = com.twilio.video.VideoView;
// const videoView = new VideoView(Utils.ad.getApplicationContext());

export class LocalVideo extends View {

    localVideoView: any;

    constructor() {
        super();
        this.localVideoView = new com.twilio.video.VideoView(Utils.ad.getApplicationContext());
    }

    get android(): any {
        return this.nativeView;
    }

    public createNativeView() {

        // return new android.widget.LinearLayout(Utils.ad.getApplicationContext());
        return this.localVideoView;

    }


    // public initNativeView(): void {

    //     // this.nativeView.addView(this.localVideoView);

    // }

    public disposeNativeView() {

        this.nativeView = null;

    }

    // public removeVideoView() {

    //     this.nativeView.removeView(this.localVideoView);

    // }




}