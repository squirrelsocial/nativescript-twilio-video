import { View } from '@nativescript/core';
export declare class LocalVideo extends View {
    localVideoView: any;
    constructor();
    get android(): any;
    createNativeView(): any;
    disposeNativeView(): void;
}
